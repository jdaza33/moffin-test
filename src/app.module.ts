import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { BikesModule } from './bikes/bikes.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.URL_DATABASE_MONGO),
    BikesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
