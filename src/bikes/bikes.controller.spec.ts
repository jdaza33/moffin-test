import { Test, TestingModule } from '@nestjs/testing';
import { BikesController } from './bikes.controller';
import { BikesService } from './bikes.service';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, connect, Model } from 'mongoose';
import { Bike, BikeSchema } from './bikes.schema';
import { getModelToken } from '@nestjs/mongoose';
// import { MongooseModule } from '@nestjs/mongoose';
// import { ConfigModule } from '@nestjs/config';
// import { NestExpressApplication } from '@nestjs/platform-express';
// import * as supertest from 'supertest';

describe('BikesController', () => {
  let controller: BikesController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let bikeModel: Model<Bike>;
  const testData: Bike = {
    id: 2,
    name: '(GDL-001) C. Epigmenio Glez./ Av. 16 de Sept.',
    obcn: 'GDL-001',
    location: 'POLÍGONO CENTRAL',
    latitude: 20.666378,
    longitude: -103.34882,
    status: 'IN_SERVICE',
  };

  beforeEach(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    bikeModel = mongoConnection.model(Bike.name, BikeSchema);
    const app: TestingModule = await Test.createTestingModule({
      controllers: [BikesController],
      providers: [
        BikesService,
        { provide: getModelToken(Bike.name), useValue: bikeModel },
      ],
    }).compile();

    controller = app.get<BikesController>(BikesController);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  // afterEach(async () => {
  //   const collections = mongoConnection.collections;
  //   for (const key in collections) {
  //     const collection = collections[key];
  //     await collection.deleteMany({});
  //   }
  // });

  describe('/stations', () => {
    it('find one bike', async () => {
      await bikeModel.create(testData);

      const bikes = await controller.find(
        '?latitude=20.666378&longitude=-103.34882&distance=1',
      );
      const bikeId = bikes[0].id;
      await bikeModel.deleteMany({});
      await mongoConnection.close();
      expect(bikeId).toEqual(2);
    });
    
    it('not find bikes', async () => {
      const bikes = await controller.find(
        '?latitude=50&longitude=50&distance=5',
      );

      expect(bikes).toEqual([]);
    });
  });
});
