import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Bike } from './bikes.schema';

@Injectable()
export class BikesService {
  constructor(@InjectModel(Bike.name) private bikeModel: Model<Bike>) {}

  async find(lat: number, lon: number, __distance: number): Promise<Bike[]> {
    const bikes = await this.bikeModel.aggregate([
      {
        $addFields: {
          R: { $literal: 6371 },
          radio: {
            $literal: Math.PI / 180,
          },
        },
      },
      {
        $addFields: {
          dLat: {
            $multiply: ['$radio', { $subtract: [lat, '$latitude'] }],
          },
          dLon: {
            $multiply: ['$radio', { $subtract: [lon, '$longitude'] }],
          },
        },
      },
      {
        $addFields: {
          a1: {
            $sin: { $divide: ['$dLat', 2] },
          },
          a2: {
            $cos: { $multiply: ['$radio', '$latitude'] },
          },
          a3: {
            $cos: { $multiply: ['$radio', lat] },
          },
          a4: {
            $sin: { $divide: ['$dLon', 2] },
          },
        },
      },
      {
        $addFields: {
          a: {
            $sum: [
              { $pow: ['$a1', 2] },
              { $multiply: ['$a2', '$a3', { $pow: ['$a4', 2] }] },
            ],
          },
        },
      },
      {
        $addFields: {
          c: {
            $multiply: [
              2,
              {
                $atan2: [{ $sqrt: '$a' }, { $sqrt: { $subtract: [1, '$a'] } }],
              },
            ],
          },
        },
      },
      {
        $addFields: {
          distance: {
            $multiply: ['$c', '$R'],
          },
        },
      },
      {
        $match: {
          distance: { $lte: __distance },
          status: { $eq: 'IN_SERVICE' },
        },
      },
      {
        $project: {
          id: 1,
          name: 1,
          obcn: 1,
          location: 1,
          longitude: 1,
          latitude: 1,
          status: 1,
        },
      },
    ]);

    return bikes.length > 0 ? bikes : [];
  }
}
