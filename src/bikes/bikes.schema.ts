import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type BikeDocument = mongoose.HydratedDocument<Bike>;

@Schema()
export class Bike {
  @Prop()
  id: number;

  @Prop()
  name: string;

  @Prop()
  obcn: string;

  @Prop()
  location: string;

  @Prop()
  latitude: number;

  @Prop()
  longitude: number;

  @Prop({ enum: ['IN_SERVICE', 'NOT_IN_SERVICE'] })
  status: string;
}

export const BikeSchema = SchemaFactory.createForClass(Bike);
