import { Controller, Get, Query } from '@nestjs/common';
import { BikesService } from './bikes.service';

@Controller('stations')
export class BikesController {
  constructor(private readonly bikesService: BikesService) {}

  @Get()
  find(@Query() query) {
    return this.bikesService.find(
      parseFloat(query.latitude),
      parseFloat(query.longitude),
      parseInt(query.distance),
    );
  }
}
