# Prueba de Trabajo | NodeJS | Version 2

REST API para una prueba de trabajo creado por José Bolívar.

Tecnologías: NodeJS, NestJS, Typescript

Base de datos: MongoDB

## Install

`npm install`

## Run the app

`npm run start --> running on port 3000 by default`

## Run test

`npm test`

# REST API

## Buscar bicicletas cercanas a una coordenada

### Request

`GET /stations`

### Query params

`longitude --> Longitud`
`latitude--> Latitud`
`distance--> Distancia en kilómetros`

    curl -i -H 'Accept: application/json' http://localhost:3000/stations?longitude=-103.35305898513403&latitude=20.671032088262155&distance=500

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    [...bikes]
